package com.youngcms.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.youngcms.bean.AdvertPosition;
import com.youngcms.dao.AdvertPositionMapper;
import com.youngcms.dao.base.MapperSupport;
import com.youngcms.service.base.impl.ServiceSupport;

@Service
public class AdvertPositionService extends ServiceSupport<AdvertPosition> {
	@Autowired
	private AdvertPositionMapper advertPositionMapper;

	@Override
	public MapperSupport<AdvertPosition> getMapperSupport() {
		return advertPositionMapper;
	}

}
