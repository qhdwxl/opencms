package com.youngcms.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.youngcms.bean.ContentVideo;
import com.youngcms.dao.ContentVideoMapper;
import com.youngcms.dao.base.MapperSupport;
import com.youngcms.service.base.impl.ServiceSupport;
@Service
public class ContentVideoService extends ServiceSupport<ContentVideo> {

	@Autowired
	private ContentVideoMapper contentVideoMapper;

	@Override
	public MapperSupport<ContentVideo> getMapperSupport() {
		return contentVideoMapper;
	}
	
	
}
