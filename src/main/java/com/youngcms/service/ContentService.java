package com.youngcms.service;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.youngcms.bean.Content;
import com.youngcms.bean.ContentImage;
import com.youngcms.bean.ContentOption;
import com.youngcms.bean.ContentText;
import com.youngcms.bean.ContentVideo;
import com.youngcms.bean.Model;
import com.youngcms.dao.ContentImageMapper;
import com.youngcms.dao.ContentMapper;
import com.youngcms.dao.ContentOptionMapper;
import com.youngcms.dao.ContentTextMapper;
import com.youngcms.dao.ContentVideoMapper;
import com.youngcms.dao.ModelMapper;
import com.youngcms.dao.base.MapperSupport;
import com.youngcms.service.base.impl.ServiceSupport;
import com.youngcms.utils.DictUtil;
@Service
public class ContentService extends ServiceSupport<Content> {

	@Autowired
	private ContentMapper contentMapper;
	@Autowired
	private ModelMapper modelMapper;
	@Autowired
	private ContentOptionMapper contentOptionMapper;
	@Autowired
	private ContentVideoMapper contentVideoMapper;
	@Autowired
	private ContentImageMapper contentImageMapper;
	@Autowired
	private ContentTextMapper contentTextMapper;

	@Override
	public MapperSupport<Content> getMapperSupport() {
		return contentMapper;
	}
	
	@Transactional(propagation=Propagation.REQUIRED,rollbackFor=Exception.class)
	public void saveOrUpdate(Content content,HttpServletRequest request){
		try {
			if(content.getId()!=null){
				contentMapper.updateByPrimaryKeySelective(content);
			}else{
				contentMapper.insertSelective(content);
			}
			Model model=modelMapper.selectByPrimaryKey(content.getModelId());
			Map<String, Object> params=new HashMap<String,Object>();
			params.put("contentId", content.getId());
			if(model.getHasContent()==DictUtil.getIdByNameAndEnName("hasContent", "是")){
				contentTextMapper.deleteByParams(params);
				String contentT=request.getParameter("contentText");
				ContentText contentText=new ContentText();
				contentText.setText(contentT);
				contentText.setContentId(content.getId());
				contentTextMapper.insertSelective(contentText);
			}
			if(model.getHasGroupImages()==DictUtil.getIdByNameAndEnName("hasGroupImages", "是")){
				contentImageMapper.deleteByParams(params);
				String imagePathAll=request.getParameter("imagePathAll");
				String imageNameAll=request.getParameter("imageNameAll");
				String[] imagePath=StringUtils.split(imagePathAll, ";");
				String[] imageName=StringUtils.split(imageNameAll, ";");
				for(int i=0;i<imagePath.length;i++){
					if(StringUtils.isNotBlank(imagePath[i])){
						ContentImage contentImage=new ContentImage();
						contentImage.setClickNum(0);
						contentImage.setTitle(imageName[i]);
						contentImage.setImageUrl(imagePath[i]);
						contentImage.setContentId(content.getId());
						contentImageMapper.insertSelective(contentImage);
					}
				}
			}
			if(model.getHasVedio()==DictUtil.getIdByNameAndEnName("hasVedio", "是")){
				contentVideoMapper.deleteByParams(params);
				String vedioUrl=request.getParameter("vedioUrl");
				ContentVideo contentVideo=new ContentVideo();
				contentVideo.setVideoUrl(vedioUrl);
				contentVideo.setContentId(content.getId());
				contentVideoMapper.insertSelective(contentVideo);
			}
			if(model.getHasOptions()==DictUtil.getIdByNameAndEnName("hasOptions", "是")){
				contentOptionMapper.deleteByParams(params);
				String optionPathAll=request.getParameter("optionPathAll");
				String optionNameAll=request.getParameter("optionNameAll");
				String[] optionPath=StringUtils.split(optionPathAll, ";");
				String[] optionName=StringUtils.split(optionNameAll, ";");
				for(int i=0;i<optionPath.length;i++){
					if(StringUtils.isNotBlank(optionPath[i])){
						ContentOption contentOption=new ContentOption();
						contentOption.setClickNum(0);
						contentOption.setTitle(optionPath[i]);
						contentOption.setOptionUrl(optionName[i]);
						contentOption.setContentId(content.getId());
						contentOptionMapper.insertSelective(contentOption);
					}
				}
			}
		} catch (Exception e) {
			System.out.println(e.toString());
		}
	}
	
}
