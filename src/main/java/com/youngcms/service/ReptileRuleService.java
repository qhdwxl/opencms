package com.youngcms.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.youngcms.bean.ReptileRule;
import com.youngcms.dao.ReptileRuleMapper;
import com.youngcms.dao.base.MapperSupport;
import com.youngcms.service.base.impl.ServiceSupport;
@Service
public class ReptileRuleService extends ServiceSupport<ReptileRule> {

	@Autowired
	private ReptileRuleMapper reptileRuleMapper;

	@Override
	public MapperSupport<ReptileRule> getMapperSupport() {
		return reptileRuleMapper;
	}
	
	
}
