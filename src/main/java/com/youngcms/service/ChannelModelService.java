package com.youngcms.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.youngcms.bean.ChannelModel;
import com.youngcms.dao.ChannelModelMapper;
import com.youngcms.dao.base.MapperSupport;
import com.youngcms.service.base.impl.ServiceSupport;
@Service
public class ChannelModelService extends ServiceSupport<ChannelModel> {

	@Autowired
	private ChannelModelMapper channelModelMapper;

	@Override
	public MapperSupport<ChannelModel> getMapperSupport() {
		return channelModelMapper;
	}
	
	
}
