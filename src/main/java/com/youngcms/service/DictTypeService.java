package com.youngcms.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.youngcms.bean.DictType;
import com.youngcms.dao.DictTypeMapper;
import com.youngcms.dao.base.MapperSupport;
import com.youngcms.service.base.impl.ServiceSupport;
@Service
public class DictTypeService extends ServiceSupport<DictType> {
   @Autowired
   private DictTypeMapper dictTypeMapper;

@Override
public MapperSupport<DictType> getMapperSupport() {
	return dictTypeMapper;
}
}
