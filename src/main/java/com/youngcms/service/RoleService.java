package com.youngcms.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.youngcms.bean.Role;
import com.youngcms.dao.RoleMapper;
import com.youngcms.dao.base.MapperSupport;
import com.youngcms.service.base.impl.ServiceSupport;
@Service
public class RoleService extends ServiceSupport<Role> {
   @Autowired
   private RoleMapper roleMapper;

@Override
public MapperSupport<Role> getMapperSupport() {
	// TODO Auto-generated method stub
	return roleMapper;
}
}
