package com.youngcms.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.youngcms.bean.ContentImage;
import com.youngcms.dao.ContentImageMapper;
import com.youngcms.dao.base.MapperSupport;
import com.youngcms.service.base.impl.ServiceSupport;
@Service
public class ContentImageService extends ServiceSupport<ContentImage> {

	@Autowired
	private ContentImageMapper contentImageMapper;

	@Override
	public MapperSupport<ContentImage> getMapperSupport() {
		return contentImageMapper;
	}
	
	
}
