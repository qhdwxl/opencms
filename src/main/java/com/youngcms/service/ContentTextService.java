package com.youngcms.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.youngcms.bean.ContentText;
import com.youngcms.dao.ContentTextMapper;
import com.youngcms.dao.base.MapperSupport;
import com.youngcms.service.base.impl.ServiceSupport;
@Service
public class ContentTextService extends ServiceSupport<ContentText> {

	@Autowired
	private ContentTextMapper contentTextMapper;

	@Override
	public MapperSupport<ContentText> getMapperSupport() {
		return contentTextMapper;
	}
	
	
}
