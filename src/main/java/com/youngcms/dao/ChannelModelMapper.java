package com.youngcms.dao;

import com.youngcms.bean.ChannelModel;
import com.youngcms.dao.base.MapperSupport;

public interface ChannelModelMapper extends MapperSupport<ChannelModel> {
}