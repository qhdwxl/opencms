package com.youngcms.dao;

import com.youngcms.bean.Model;
import com.youngcms.dao.base.MapperSupport;

public interface ModelMapper extends MapperSupport<Model> {
}