package com.youngcms.dao;

import com.youngcms.bean.Channel;
import com.youngcms.dao.base.MapperSupport;

public interface ChannelMapper extends MapperSupport<Channel>  {

	Channel selectByCode(String code);

}