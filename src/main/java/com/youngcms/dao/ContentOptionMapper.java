package com.youngcms.dao;

import com.youngcms.bean.ContentOption;
import com.youngcms.dao.base.MapperSupport;

public interface ContentOptionMapper  extends MapperSupport<ContentOption> {
}