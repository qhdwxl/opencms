package com.youngcms.dao;

import com.youngcms.bean.Content;
import com.youngcms.dao.base.MapperSupport;

public interface ContentMapper extends MapperSupport<Content> {
}