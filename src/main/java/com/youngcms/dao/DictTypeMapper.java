package com.youngcms.dao;

import com.youngcms.bean.DictType;
import com.youngcms.dao.base.MapperSupport;

public interface DictTypeMapper extends MapperSupport<DictType> {
}