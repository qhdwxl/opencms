package com.youngcms.bean;

import com.youngcms.vo.PageView;

public class Role extends PageView<Role> {
    private Integer id;

    private String name;

    private String moduleIds;

    private String moduleNames;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getModuleIds() {
        return moduleIds;
    }

    public void setModuleIds(String moduleIds) {
        this.moduleIds = moduleIds == null ? null : moduleIds.trim();
    }

    public String getModuleNames() {
        return moduleNames;
    }

    public void setModuleNames(String moduleNames) {
        this.moduleNames = moduleNames == null ? null : moduleNames.trim();
    }
}