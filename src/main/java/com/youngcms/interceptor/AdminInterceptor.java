package com.youngcms.interceptor;

import java.io.Writer;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.youngcms.annotation.PassCheckAnnotation;
import com.youngcms.bean.Module;
import com.youngcms.bean.SysUser;
import com.youngcms.controller.base.AdminBaseController;
import com.youngcms.utils.SessionKey;
import com.youngcms.vo.ActionResult;

public class AdminInterceptor implements HandlerInterceptor {

	@Override
	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object object, Exception exception)
			throws Exception {
		
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response,
			Object object, ModelAndView modelAndView) throws Exception {
		
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
			Object object) throws Exception {
		String url=request.getRequestURI();
		if(object instanceof HandlerMethod){
			HandlerMethod method=(HandlerMethod) object;
			PassCheckAnnotation passCheckAnnotation=method.getMethodAnnotation(PassCheckAnnotation.class);
			if(method.getBean() instanceof AdminBaseController){
					if(request.getSession(true).getAttribute(SessionKey.SYS_USER)==null){
					   request.getRequestDispatcher("/admin/loginForm").forward(request, response);
					   return false;
					}else{
						if(passCheckAnnotation==null){
						Boolean b=true;
						Map<String, Module> map=(Map<String, Module>) request.getSession(true).getAttribute(SessionKey.SYS_MODULE);
						for (Entry<String, Module> entry : map.entrySet()) {
							Module module=entry.getValue();
							if(url.contains(module.getHref())){
								b=true;
								break;
							}
						}
						SysUser sysUser=(SysUser) request.getSession(true).getAttribute(SessionKey.SYS_USER);
						String loginName=sysUser.getLoginName();
						if(!"admin".equals(loginName) && (url.contains("delete") || url.contains("addOrUpdate") )){
							b=false;
						}
						String requestType = request.getHeader("X-Requested-With");  
						if(!b){
							if("XMLHttpRequest".equals(requestType)){
								response.setContentType("text/xml; charset=utf-8");
								ActionResult result=new ActionResult();
								result.setSuccess(false);
								result.setMessage("非常遗憾，您没有权限操作");
								JSONObject json=JSONObject.fromObject(result);
								Writer out=response.getWriter();
								out.write(json.toString());
							}else{
								request.getRequestDispatcher("/view/admin/error.jsp").forward(request, response);
							}
							    return false;
						}
					}
				}
			}
		}
		return true;
	}

}
