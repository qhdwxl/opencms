package com.youngcms.freemarker;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.youngcms.bean.Channel;
import com.youngcms.freemarker.utils.Freemarker;
import com.youngcms.service.ChannelService;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import freemarker.template.WrappingTemplateModel;
/**
 * 栏目列表
 * @author fumiao-pc
 *
 */
@Repository
public class ChannelList implements TemplateDirectiveModel {
	
	@Autowired
	private ChannelService channelService;

	@Override
	public void execute(Environment env, Map map, TemplateModel[] loopVars,
			TemplateDirectiveBody body) throws TemplateException, IOException {
		Integer pId=Freemarker.getInteger(map, "pId");
		Integer size=Freemarker.getInteger(map, "size");
		Integer isNav=Freemarker.getInteger(map, "isNav");
		String fields=Freemarker.getString(map, "fields");
		String sort=Freemarker.getString(map, "sort");
     	Channel channel=new Channel();
     	if(pId!=null){
     		channel.setpId(pId);
     	}
     	if(isNav!=null){
     		channel.setIsNav(1);
     	}
     	if(size!=null){
     		channel.setPageRow(size);
     	}
     	if(StringUtils.isNotBlank(fields)){
			String orderBy="";
			String[] f=StringUtils.split(fields,",");
			String[] s=StringUtils.split(sort,",");
			for(int i=0;i<f.length;i++){
				orderBy=f[i]+" "+s[i];
			}
			channel.setOrderBy("order by "+orderBy);
		}
		List<Channel> list=channelService.list(channel).getQueryResult();
		loopVars[0]= WrappingTemplateModel.getDefaultObjectWrapper().wrap(list);
		body.render(env.getOut());
	}

}
