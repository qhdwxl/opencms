package com.youngcms.controller.admin;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.youngcms.annotation.PassCheckAnnotation;
import com.youngcms.bean.Module;
import com.youngcms.controller.base.AdminBaseController;
import com.youngcms.service.ModuleService;
import com.youngcms.utils.Constants;
import com.youngcms.utils.SessionKey;
import com.youngcms.vo.Menu;

@Controller
@RequestMapping("/admin/")
public class AdminIndexController extends AdminBaseController{
	@Autowired
	private ModuleService moduleService;
	
	@RequestMapping("index")
	@PassCheckAnnotation
	public ModelAndView list(HttpServletRequest request,ModelMap mode) throws Exception{
		
		ModelAndView mnv = new ModelAndView();
		mnv.setViewName("view/admin/index");
		return mnv;
		
	}
	
	@RequestMapping("left")
	@PassCheckAnnotation
	public ModelAndView left(HttpServletRequest request,ModelMap mode) throws Exception{
		ModelAndView mnv = new ModelAndView();
		mnv.setViewName("view/admin/left");
		return mnv;
		
	}
	
	@RequestMapping("top")
	@PassCheckAnnotation
	public ModelAndView head(HttpServletRequest request,ModelMap mode) throws Exception{
		
		ModelAndView mnv = new ModelAndView();
		mnv.setViewName("view/admin/top");
		return mnv;
		
	}
	
	@RequestMapping("main")
	@PassCheckAnnotation
	public ModelAndView main(HttpServletRequest request,ModelMap mode) throws Exception{
		
		ModelAndView mnv = new ModelAndView();
		mnv.setViewName("view/admin/main");
		return mnv;
		
	}
	
	@RequestMapping("footer")
	@PassCheckAnnotation
	public ModelAndView foot(HttpServletRequest request,ModelMap mode) throws Exception{
		
		ModelAndView mnv = new ModelAndView();
		mnv.setViewName("view/admin/footer");
		return mnv;
		
	}
	
	@RequestMapping("getModules")
	@PassCheckAnnotation
	@ResponseBody
	public List<Menu> getModules(HttpServletRequest request,ModelMap mode) throws Exception{
		String pid=request.getParameter("pid");
		List<Menu> menus=new ArrayList<Menu>();
		Map<String, Module> modules=(Map<String, Module>) request.getSession(true).getAttribute(SessionKey.SYS_MODULE);
		for (Entry<String, Module> entry : modules.entrySet()) {
			Module module=entry.getValue();
			if(Constants.MODULU_TYPE_MENU.equals(module.getModuleType())){
				if(module.getpId()!=null && module.getpId().toString().equals(pid)){
					Menu menu=new Menu();
					menu.setId(module.getId());
					menu.setName(module.getName());
					menu.setUrl(module.getHref());
					menu.setTarget(module.getTarget());
					menu.setIcon(module.getIcon());
					menu.setChildren(getChildren(module.getId(),modules));
					menus.add(menu);
				}
			}
		}
		return menus;
	}
	
	private List<Menu> getChildren(Integer id, Map<String, Module> modules) {
		List<Menu> menus=new ArrayList<Menu>();
		for (Entry<String, Module> entry : modules.entrySet()) {
			Module module=entry.getValue();
			if(module.getpId() !=null && id==module.getpId()){
				if(Constants.MODULU_TYPE_MENU.equals(module.getModuleType())){
					Menu menu=new Menu();
					menu.setId(module.getId());
					menu.setName(module.getName());
					menu.setUrl(module.getHref());
					menu.setTarget(module.getTarget());
					menu.setIcon(module.getIcon());
					menus.add(menu);
				}
			}
		}
		return menus;
	}
	
}
