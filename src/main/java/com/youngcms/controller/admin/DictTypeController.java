package com.youngcms.controller.admin;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.youngcms.bean.DictType;
import com.youngcms.controller.base.AdminBaseController;
import com.youngcms.service.DictTypeService;
import com.youngcms.vo.ActionResult;
import com.youngcms.vo.QueryResult;
@Controller
@RequestMapping("/admin/dictType/")
@SessionAttributes("pageForm")
public class DictTypeController extends AdminBaseController{

     @Autowired
     private DictTypeService dictTypeService;

     @RequestMapping("list")
     public ModelAndView list(DictType dictType,HttpServletRequest request) throws Exception{
        QueryResult<DictType> queryResult=dictTypeService.list(dictType);
        dictType.setPageDate(queryResult.getQueryResult());
        dictType.setTotalCount(queryResult.getCount());
        ModelAndView mnv = new ModelAndView();
        mnv.addObject(DEFAULT_PAGE_VIEW,dictType);
        mnv.setViewName("view/admin/dictType/list");
        return mnv;
     }

     @RequestMapping("delete")
     @ResponseBody
     public ActionResult delete(DictType dictType,HttpServletRequest request) throws Exception{
        ActionResult result=new ActionResult();
       try {
	     dictTypeService.deleteByPrimaryKey(dictType.getId());
	     result.setSuccess(true);
	     result.setMessage(RESULE_SUCCESS_DELETE);
		 } catch (Exception e) {
			result.setSuccess(false);
			result.setMessage(RESULE_ERROR_DELETE);
		}
		return result;
	 }

	 @RequestMapping("showForm")
	 public ModelAndView addForm(DictType dictType,HttpServletRequest request) throws Exception{
		ModelAndView mnv = new ModelAndView();
		if(dictType.getId()!=null){
			dictType=dictTypeService.selectByPrimaryKey(dictType.getId());
		}
		mnv.addObject(DEFAULT_PAGE_FORM, dictType);
		mnv.setViewName("view/admin/dictType/form");
		return mnv;
	 }

	@RequestMapping("addOrUpdate")
	@ResponseBody
	public ActionResult add(DictType dictType,HttpServletRequest request) throws Exception{
		ActionResult result=new ActionResult();
		try {
			dictTypeService.saveOrUpdate(dictType);
			result.setSuccess(true);
			result.setMessage(RESULE_SUCCESS);
		} catch (Exception e) {
			result.setSuccess(false);
			result.setMessage(RESULE_ERROR);
		}
		return result;
	}
}
