package com.youngcms.controller.admin;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.youngcms.bean.Channel;
import com.youngcms.bean.Content;
import com.youngcms.bean.ContentImage;
import com.youngcms.bean.ContentOption;
import com.youngcms.bean.ContentText;
import com.youngcms.bean.ContentVideo;
import com.youngcms.bean.Model;
import com.youngcms.controller.base.AdminBaseController;
import com.youngcms.freemarker.utils.Freemarker;
import com.youngcms.service.ChannelService;
import com.youngcms.service.ContentImageService;
import com.youngcms.service.ContentOptionService;
import com.youngcms.service.ContentService;
import com.youngcms.service.ContentTextService;
import com.youngcms.service.ContentVideoService;
import com.youngcms.service.ModelService;
import com.youngcms.utils.Constants;
import com.youngcms.utils.DictUtil;
import com.youngcms.vo.ActionResult;
import com.youngcms.vo.ContentDetail;
import com.youngcms.vo.QueryResult;
@Controller
@RequestMapping("/admin/html/")
public class HtmlController extends AdminBaseController{

     @Autowired
     private ChannelService channelService;
     @Autowired
     private ContentService contentService;
     @Autowired
     private ModelService modelService;
     @Autowired
     private ContentTextService contentTextService;
     @Autowired
     private ContentImageService contentImageService;
     @Autowired
     private ContentOptionService contentOptionService;
     @Autowired
     private ContentVideoService contentVideoService;

     @RequestMapping("page")
     public ModelAndView channel(Channel channel,HttpServletRequest request) throws Exception{
        ModelAndView mnv = new ModelAndView();
        mnv.setViewName("view/admin/html/page");
        return mnv;
     }
     
     @RequestMapping("channel")
     @ResponseBody
     public Object channelCreate(HttpServletRequest request) throws Exception{
    	ActionResult result=new ActionResult();
    	try {
    		String channelIds=request.getParameter("channelIds");
        	String[] channels=StringUtils.split(channelIds,",");
        	for(int i=0;i<channels.length;i++){
        		String channelId=channels[i];
        		if(StringUtils.isNotBlank(channelId)){
        			Channel channel=channelService.selectByPrimaryKey(Integer.valueOf(channelId));
        			if(StringUtils.isNotBlank(channel.getCode()) && StringUtils.isNotBlank(channel.getTemplate())){
        				String htmlPath=context.getRealPath("/static/"+Constants.TEMPLATE_FOLDER+"/channel/"+channel.getCode()+".html");
        				data.put("channel", channel);
            	    	Freemarker.crateHTML(freemarkerCfg,data, channel.getTemplate(), htmlPath);
        			}
        		}
        	}
        	result.setSuccess(true);
    	    result.setMessage(RESULE_SUCCESS);
		} catch (Exception e) {
			result.setSuccess(false);
		    result.setMessage(RESULE_ERROR);
		}
	    return result;
     }
     
     @RequestMapping("content")
     @ResponseBody
     public Object contentCreate(HttpServletRequest request) throws Exception{
    	 ActionResult result=new ActionResult();
     	try {
     		String channelIds=request.getParameter("channelIds");
         	String[] channels=StringUtils.split(channelIds,",");
         	Map<String, Object> params=new HashMap<String, Object>();
         	params.put("channels", channels);
         	QueryResult<Content> contents=contentService.list(params);
         	for(int i=0;i<contents.getQueryResult().size();i++){
         		Content content=contents.getQueryResult().get(i);
         		ContentDetail contentDetail=new ContentDetail();
         		BeanUtils.copyProperties(contentDetail,content);
         		Model model=modelService.selectByPrimaryKey(content.getModelId());
         		params.put("contentId", content.getId());
    			if(model.getHasContent()==DictUtil.getIdByNameAndEnName("hasContent", "是")){
    			   QueryResult<ContentText> queryResult=contentTextService.list(params);
    			   if(queryResult.getCount()>0){
    				   ContentText contentText=queryResult.getQueryResult().get(0);
    				   contentDetail.setContentText(contentText);
    				}
    			}
    			if(model.getHasGroupImages()==DictUtil.getIdByNameAndEnName("hasGroupImages", "是")){
    				QueryResult<ContentImage> queryResult=contentImageService.list(params);
    				contentDetail.setContentImages(queryResult.getQueryResult());
    			}
    			if(model.getHasVedio()==DictUtil.getIdByNameAndEnName("hasVedio", "是")){
    				QueryResult<ContentVideo> queryResult=contentVideoService.list(params);
    				if(queryResult.getCount()>0){
    					ContentVideo contentVideo=queryResult.getQueryResult().get(0);
    					contentDetail.setContentVideo(contentVideo);
    				}
    			}
    			if(model.getHasOptions()==DictUtil.getIdByNameAndEnName("hasOptions", "是")){
    				QueryResult<ContentOption> queryResult=contentOptionService.list(params);
    				contentDetail.setContentOptions(queryResult.getQueryResult());
    			}
    			String htmlPath=context.getRealPath("/static/"+Constants.TEMPLATE_FOLDER+"/content/"+contentDetail.getId()+".html");
				data.put("contentDetail", contentDetail);
    	    	Freemarker.crateHTML(freemarkerCfg,data, contentDetail.getTemplate(), htmlPath);
         	}
         	result.setSuccess(true);
     	    result.setMessage(RESULE_SUCCESS);
 		} catch (Exception e) {
 			result.setSuccess(false);
 		    result.setMessage(RESULE_ERROR);
 		}
 	    return result;
     }
}
