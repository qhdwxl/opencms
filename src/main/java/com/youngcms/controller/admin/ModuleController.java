package com.youngcms.controller.admin;
import java.util.LinkedHashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.youngcms.annotation.PassCheckAnnotation;
import com.youngcms.bean.Module;
import com.youngcms.bean.Role;
import com.youngcms.controller.base.AdminBaseController;
import com.youngcms.service.ModuleService;
import com.youngcms.service.RoleService;
import com.youngcms.utils.ListTypeParameter;
import com.youngcms.vo.ActionResult;
import com.youngcms.vo.QueryResult;
@Controller
@RequestMapping("/admin/module/")
@SessionAttributes("pageForm")
public class ModuleController extends AdminBaseController{

     @Autowired
     private ModuleService moduleService;
     @Autowired
     private RoleService roleService;
     
     @RequestMapping("simpleNodes")
     @ResponseBody
     public List<Module> simpleNodes(Module module,HttpServletRequest request) throws Exception{
    	 String roleId=request.getParameter("roleId");
    	 Role role=null;
    	 if(StringUtils.isNotBlank(roleId)){
 			  role=roleService.selectByPrimaryKey(Integer.valueOf(roleId));
 		 }
    	 QueryResult<Module> queryResult=moduleService.list();
    		 for(int i=0;i<queryResult.getQueryResult().size();i++){
    			 Module module2=queryResult.getQueryResult().get(i);
    			 if(module2.getpId()!=null){
    				isCheck(role,module2);
    			 }else{
    				isCheck(role,module2);
    				//module2.setOpen(true);
    			 }
    		 }
        return queryResult.getQueryResult();
     }
     
     private void isCheck(Role role, Module module) {
    	 if(role!=null){
    		 String[] ids=role.getModuleIds().split(",");
    		 Boolean b=false;
    		 for(int i=0;i<ids.length;i++){
    			 if((module.getId().toString()).equals(ids[i])){
    				 b=true;
    			 }
    		 }
    		 if(b){
    			 module.setChecked(true);
    		 }
    	 }
	}

	@RequestMapping("list")
     public ModelAndView list(Module module,HttpServletRequest request) throws Exception{
    	QueryResult<Module> queryResult=moduleService.list(module);
    	module.setPageDate(queryResult.getQueryResult());
    	module.setTotalCount(queryResult.getCount());
        ModelAndView mnv = new ModelAndView();
        mnv.addObject(DEFAULT_PAGE_VIEW,module);
        mnv.setViewName("view/admin/module/list");
        return mnv;
     }

     @RequestMapping("dataPage")
     @PassCheckAnnotation
     public ModelAndView dataPage(Module module,HttpServletRequest request) throws Exception{
    	LinkedHashMap<String, String> orderBy=new LinkedHashMap<String, String>();
    	orderBy.put("sort", "desc");
        QueryResult<Module> queryResult=moduleService.list(module);
        module.setPageDate(queryResult.getQueryResult());
        module.setTotalCount(queryResult.getCount());
        ModelAndView mnv = new ModelAndView();
        mnv.addObject(DEFAULT_PAGE_VIEW,module);
        mnv.setViewName("view/admin/module/dataPage");
        return mnv;
     }
     
     @RequestMapping("getChildren")
     @ResponseBody
     public ActionResult getChildren(Module module,HttpServletRequest request) throws Exception{
    	ActionResult result=new ActionResult();
    	module.setpId(module.getId());
    	LinkedHashMap<String, String> orderBy=new LinkedHashMap<String, String>();
    	orderBy.put("sort", "desc");
 		QueryResult<Module> queryResult=moduleService.list();
 		result.setSuccess(true);
 		result.setResult(queryResult.getQueryResult());
        return result;
     }

     @RequestMapping("delete")
     @ResponseBody
     public ActionResult delete(Module module,HttpServletRequest request) throws Exception{
        ActionResult result=new ActionResult();
       try {
	     moduleService.deleteByPrimaryKey(module.getId());
	     result.setSuccess(true);
	     result.setMessage(RESULE_SUCCESS_DELETE);
		 } catch (Exception e) {
			result.setSuccess(false);
			result.setMessage(RESULE_ERROR_DELETE);
		}
		return result;
	 }

	 @RequestMapping("showForm")
	 public ModelAndView addForm(Module module,HttpServletRequest request) throws Exception{
		String parent_module_id=request.getParameter("parent_module_id");
	    if(StringUtils.isBlank(parent_module_id)){
	    	parent_module_id="1";
	    }
	    Module pModule=moduleService.selectByPrimaryKey(Integer.valueOf(parent_module_id));
	    module.setpName(pModule.getName());
	    module.setpId(pModule.getId());
		ModelAndView mnv = new ModelAndView();
		if(module.getId()!=null){
			module=moduleService.selectByPrimaryKey(module.getId());
		}
		mnv.addObject(DEFAULT_PAGE_FORM, module);
		mnv.addObject("MODULE_TYPE_LIST", ListTypeParameter.moduleTypeList);
		mnv.setViewName("view/admin/module/form");
		return mnv;
	 }

	@RequestMapping("addOrUpdate")
	@ResponseBody
	public ActionResult add(Module module,HttpServletRequest request) throws Exception{
		ActionResult result=new ActionResult();
		try {
			moduleService.saveOrUpdate(module);
			result.setSuccess(true);
			result.setMessage(RESULE_SUCCESS);
		} catch (Exception e) {
			result.setSuccess(false);
			result.setMessage(RESULE_ERROR);
		}
		return result;
	}
}
