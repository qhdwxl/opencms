<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/view/common/basePath.jsp" %>
<%@ include file="/view/admin/common/import.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文�?</title>
<script type="text/javascript">
var documentType='${source.documentType}';
var setting = {
	async: {
		enable: true,
		url:path+"/admin/document/fileTree?documentType="+documentType
	},
	data: {
		simpleData: {
			enable: true
		}
	},
	callback: {
		onClick: onClick
	}
};
$(document).ready(function(){
	$.fn.zTree.init($("#treeDemo"), setting);
});
function onClick(event, treeId, treeNode, clickFlag) {
	$("#ifreamDiv").html("<iframe id='iframepage' src='"+path+"/admin/document/show?path="+treeNode.path+"' width='100%' height='90%'></iframe>");
}	
</script>
</head>
<body>
    <form:form action="#" id="listForm" name="listForm">
    <jsp:include page="/view/admin/common/place.jsp" />
    <div class="rightinfo">
    <div style="float: left;width: 10%;height: 90%;border-right: 1px solid #cbcbcb;">
		<ul id="treeDemo" class="ztree"></ul> 
	</div>
	<div id="ifreamDiv"  style="float: left;width: 89%;margin: 0;padding: 0;">
	</div>
    </div>
    </form:form>
</body>
</html>
