<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/view/common/basePath.jsp" %>
<%@ include file="/view/admin/common/import.jsp" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<style type="text/css">
</style>
</head>
<body>
    <textarea id="code" name="code">${fileCode }</textarea>
    <script>
        var editor = CodeMirror.fromTextArea(document.getElementById("code"), {  // 标识到textarea
        	mode: "text/html",
        	indentUnit : 2,  // 缩进单位，默认2
            smartIndent : true,  // 是否智能缩进
            styleActiveLine: true, //line选择是是否加亮
            tabSize : 4,  // Tab缩进，默认4
            readOnly : false,  // 是否只读，默认false
            showCursorWhenSelecting : true,
            lineNumbers : true,
            width: '100%',
            size: 'auto'
        });
        editor.setSize('auto', 'auto');
    </script>
</body>
</html>
