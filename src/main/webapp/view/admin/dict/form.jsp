<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/view/common/basePath.jsp" %>
<%@ include file="/view/admin/common/import.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文�?</title>
<script type="text/javascript">
$(document).ready(function(e) {
	$(".select2").uedSelect({
		width : 200  
	});
});
</script>
</head>
<body>
    <form:form action="${path}/admin/dict/addOrUpdate" id="form_add_update" name="form_add_update" modelAttribute="pageForm">
       <form:hidden path="id" />
      <table class='formtable'>
       <tr>
          <td>名称</td>
          <td><form:input path="name" cssClass="dfinput"/></td>
       </tr>
       <tr>
          <td>字典类型</td>
          <td>
          <form:select path="dictTypeId" cssClass="select">
             <form:options items="${DICT_TYPE_LIST }" itemValue="id" itemLabel="chName" />
          </form:select>
          </td>
       </tr>
        <tr>
          <td>排序</td>
          <td><form:input path="sort" cssClass="dfinput"/></td>
       </tr>
       </table>
     </form:form>
</body>
</html>
