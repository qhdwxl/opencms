<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link rel="stylesheet" media="screen" type="text/css" href="<%=request.getContextPath() %>/view/admin/resource/lib/kkpager/kkpager_blue.css" />
<script type="text/javascript" src="<%=request.getContextPath() %>/view/admin/resource/lib/kkpager/kkpager.min.js"></script>
<div style="width:800px;margin:0 auto;">
<input type="hidden" id="currentPage" name="currentPage" value="${pageView.currentPage}">
<div id="kkpager"></div>
</div>
<script type="text/javascript">
function getParameter(name) { 
	var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)"); 
	var r = window.location.search.substr(1).match(reg); 
	if (r!=null) return unescape(r[2]); return null;
}
$(function(){
	var totalPage = '${pageView.pageCount}';
	var totalRecords = '${pageView.totalCount }';
	var pageNo = '${pageView.currentPage}';
	//有些参数是可选的，比如lang，若不传有默认值
	kkpager.generPageHtml({
		pno : pageNo,
		total : totalPage,
		totalRecords : totalRecords,
		mode : 'click', //设置为click模式
		click : function(n){
			$("#currentPage").val(n);
        	document.listForm.submit();
	    }
	});
});
</script>
