<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/view/common/basePath.jsp" %>
<%@ include file="/view/admin/common/import.jsp" %>
<%@ taglib prefix="fckeditor" uri="http://java.fckeditor.net" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文�?</title>
<script type="text/javascript">
$(function(){
	var modelIds="";
	var checkboxs=$("input[type='checkbox']:checked");
	for(var i=0;i<checkboxs.length;i++){
		modelIds+=checkboxs[i].value+",";
	}
	 if(modelIds!=""){
		 modelIds=modelIds.substring(0,modelIds.length-1);	
	}
	$("#modelIds").val(modelIds);
});
function fileUploadSuccess(file){
	$("#imgUrl").val(file);
}
function chooseModel(wel){
	var modelIds="";
	var checkboxs=$("input[type='checkbox']:checked");
	for(var i=0;i<checkboxs.length;i++){
		modelIds+=checkboxs[i].value+",";
	}
	 if(modelIds!=""){
		 modelIds=modelIds.substring(0,modelIds.length-1);	
	 }
     $("#modelIds").val(modelIds);
}
</script>
</head>
<body>
    <form:form action="${path}/admin/channel/addOrUpdate" id="form_add_update" name="form_add_update" modelAttribute="pageForm">
         <form:hidden path="id" />
         <input type="hidden" id="modelIds" name="modelIds"/>
	     <table class="formtable">
	                <tr>
	                  <td colspan="4"><div class="formtitle"><span>基本信息</span></div></td>
	                </tr>
	                <tr>
					    <td>父节点名称</td>
						<td>
						   <input type="text" id="pName" name="pName"  readonly value="${pageForm.pName }"  class="dfinput"  onclick="showMenu();" datatype="required"  errormsg="不能为空" />
						   <input type="hidden" id="pId" name="pId" value="${pageForm.pId }" class="dfinput"  onclick="showMenu();" />
						</td>
						<td>名称</td>
						<td><form:input path="name" cssClass="dfinput" datatype="required"  errormsg="名称不能为空" /></td>
					</tr>
					<tr>
					    <td>缩略图</td>
						<td>
						   <form:input path="imgUrl" readonly="true" cssClass="dfinput"/>
						   <a href="javascript:fileUpload('fileUploadSuccess');" class="add">图片上传</a>
				        </td>
				        <td>模板</td>
						<td>
						    <form:select path="template" items="${templates }" itemLabel="label" itemValue="value"></form:select>
						</td>
			        </tr>
					<tr>
					    <td>页面标示</td>
						<td><form:input path="code" cssClass="dfinput" datatype="required"  errormsg="栏目标示不能为空"/></td>
						<td>排序</td>
						<td><form:input path="sort" cssClass="dfinput"/></td>
					</tr>
					<tr>
					    <td>是否外部</td>
						<td>
						    <form:radiobutton path="isOut" value="1" />是&nbsp;&nbsp;&nbsp;
						    <form:radiobutton path="isOut" value="2" />否
						</td>
						<td>链接</td>
						<td><form:input path="outUrl" cssClass="dfinput"/></td>
					</tr>
					<tr>
						<td>是否导航</td>
						<td>
						    <form:radiobutton path="isNav" value="1" />是&nbsp;&nbsp;&nbsp;
						    <form:radiobutton path="isNav" value="2" />否
						</td>
						<td>模型</td>
						<td>
						 <c:forEach items="${modelList }" var="model">
						   <span>
						        <c:set var="YN" value="0"></c:set>
						        <c:forEach items="${channelModelList }" var="channelModel">
									<c:if test="${channelModel.modelId==model.id }">
									   <c:set var="YN" value="1"></c:set>
									</c:if>      
							    </c:forEach>
							    <c:choose>
									<c:when test="${YN==1 }">
										<input type="checkbox" checked="checked" value="${model.id }" onclick="chooseModel(this)" />${model.name }
									</c:when>
									<c:otherwise>
										<input type="checkbox" value="${model.id }" onclick="chooseModel(this)" />${model.name }
									</c:otherwise>
								</c:choose>
						   </span>
						 </c:forEach>
						</td>
					</tr>
		</table>
     </form:form>
     <jsp:include page="/view/admin/content/channelTree.jsp"/>
</body>
</html>
