﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/view/common/basePath.jsp" %>
<%@ include file="/view/admin/common/import.jsp" %>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<LINK rel="Bookmark" href="/favicon.ico" >
<LINK rel="Shortcut Icon" href="/favicon.ico" />

<title>后台管理系统</title>
<meta name="keywords" content="后台管理系统">
<meta name="description" content="后台管理系统">
<link href="<%=request.getContextPath() %>/view/admin/resource/skin/default/skin.css" rel="stylesheet" type="text/css" id="skin" />
</head>
<body>
<header class="Hui-header cl"> <a class="Hui-logo l" title="H-ui.admin v2.3" href="/">后台管理系统</a> <a class="Hui-logo-m l" href="/" title="H-ui.admin">H-ui</a> <span class="Hui-subtitle l">V2.3</span>
	<nav class="mainnav cl" id="Hui-nav">
		<ul>
			<c:forEach items="${sys_menu }" var="menu" varStatus="i">
			   <c:choose>
			      <c:when test="${i.index==0 }">
			          <li class="dropDown dropDown_click open">
			             <a href="javascript:getChildren(${menu.id },this);" class="dropDown_A" value="${menu.id }">
							  <i class="${menu.icon } mr-5"></i>${menu.name }
			             </a>
			           </li>
			      </c:when>
			      <c:otherwise>
			      	  <li class="dropDown dropDown_click">
			      	    <a href="javascript:getChildren(${menu.id },this);" class="dropDown_A" value="${menu.id }">
			      	      <i class="${menu.icon } mr-5"></i>${menu.name }
			      	    </a>
			      	 </li>
			      </c:otherwise>
			   </c:choose>
		    </c:forEach>
		</ul>
	</nav>
	<ul class="Hui-userbar">
		<li>${sys_user.realName }</li>
		<li class="dropDown dropDown_hover"><a href="#" class="dropDown_A">${sys_user.loginName } <i class="Hui-iconfont">&#xe6d5;</i></a>
			<ul class="dropDown-menu radius box-shadow">
				<li><a href="#">个人信息</a></li>
				<li><a href="#">切换账户</a></li>
				<li><a href="${path }/admin/loginOut">退出</a></li>
			</ul>
		</li>
		<li id="Hui-msg"> <a href="#" title="消息"><span class="badge badge-danger">1</span><i class="Hui-iconfont" style="font-size:18px">&#xe68a;</i></a> </li>
		<li id="Hui-skin" class="dropDown right dropDown_hover"><a href="javascript:;" title="换肤"><i class="Hui-iconfont" style="font-size:18px">&#xe62a;</i></a>
			<ul class="dropDown-menu radius box-shadow">
				<li><a href="javascript:;" data-val="default" title="默认（黑色）">默认（黑色）</a></li>
				<li><a href="javascript:;" data-val="blue" title="蓝色">蓝色</a></li>
				<li><a href="javascript:;" data-val="green" title="绿色">绿色</a></li>
				<li><a href="javascript:;" data-val="red" title="红色">红色</a></li>
				<li><a href="javascript:;" data-val="yellow" title="黄色">黄色</a></li>
				<li><a href="javascript:;" data-val="orange" title="绿色">橙色</a></li>
			</ul>
		</li>
	</ul>
	</header>
<aside class="Hui-aside">
	<input runat="server" id="divScrollValue" type="hidden" value="" />
	<div class="menu_dropdown bk_2">
	</div>
</aside>
<div class="dislpayArrow"><a class="pngfix" href="javascript:void(0);" onClick="displaynavbar(this)"></a></div>
<section class="Hui-article-box">
	<div id="Hui-tabNav" class="Hui-tabNav">
		<div class="Hui-tabNav-wp">
			<ul id="min_title_list" class="acrossTab cl">
				<li class="active"><span title="我的桌面" data-href="${path }/view/admin/main.jsp">我的桌面</span><em></em></li>
			</ul>
		</div>
		<div class="Hui-tabNav-more btn-group"><a id="js-tabNav-prev" class="btn radius btn-default size-S" href="javascript:;"><i class="Hui-iconfont">&#xe6d4;</i></a><a id="js-tabNav-next" class="btn radius btn-default size-S" href="javascript:;"><i class="Hui-iconfont">&#xe6d7;</i></a></div>
	</div>
	<div id="iframe_box" class="Hui-article">
		<div class="show_iframe">
			<div style="display:none" class="loading"></div>
			<iframe scrolling="yes" frameborder="0" src="${path }/view/admin/main.jsp"></iframe>
		</div>
	</div>
</section>
<script type="text/javascript">
$(function(){
	var first=$(".mainnav").find("ul li").get(0);
	var a=$(first).find("a").get(0);
	var pid=$(a).attr("value");
	getChildren(pid);
});
function getChildren(pid){
	$.ajax({
		 type: "POST",
         url: path+"/admin/getModules",
         data: {pid:pid},
         dataType: "json",
         success: function(data){
        	 $(".bk_2").html("");
        	 var _html="";
        	 $.each(data,function(i,second){
        		_html+="<dl id='menu-tongji'>"+
    			"<dt style='font-size: 15px;background: #f2f2f2'><i class='"+second.icon+"'></i>&nbsp;"+second.name+"<i class='Hui-iconfont menu_dropdown-arrow'>&#xe6d5;</i></dt>"+
    			"<dd style='display: block;'>";
    			if(second.children.length>0){
        			_html+="<ul>";
        			$.each(second.children,function(j,third){
        				_html+="<li><a _href='"+path+""+third.url+"' href='javascript:void(0)'><i class='"+third.icon+"'></i>&nbsp;"+third.name+"</a></li>";
        			});
        			_html+="</ul>";
        		}
    			_html+="</dd></dl>";
        	});
        	$(".bk_2").append(_html);
         }
	});
}
</script>
</body>
</html>