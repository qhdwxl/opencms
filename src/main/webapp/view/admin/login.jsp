﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/view/common/basePath.jsp" %>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link href="<%=request.getContextPath() %>/view/admin/resource/css/H-ui.min.css" rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath() %>/view/admin/resource/css/H-ui.login.css" rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath() %>/view/admin/resource/lib/Hui-iconfont/1.0.1/iconfont.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<%=request.getContextPath() %>/view/admin/resource/lib/jquery/1.9.1/jquery.min.js"></script> 
<script type="text/javascript" src="<%=request.getContextPath() %>/view/admin/resource/js/H-ui.js"></script> 
<title>后台登录</title>
<meta name="keywords" content="H-ui.admin v2.3,H-ui网站后台模版,后台模版下载,后台管理系统模版,HTML后台模版下载">
<meta name="description" content="H-ui.admin v2.3，是一款由国人开发的轻量级扁平化网站后台模板，完全免费开源的网站后台管理系统模版，适合中小型CMS后台系统。">
<script language="javascript">
	function login(){
		$.ajax({
			url:path+"/admin/login",
			type:"post",
			data:$("#theform").serialize(),
			dataType:'json',
			success:function(data){
				if(data.success){
				   location.href=path+"/admin/index";	
				}else{
					$("#errorMsg").html(data.message);
				}
			}
		});
	}
	function changeCode(){
		var getTimestamp=new Date().getTime();
		$("#code").attr("src",path+"/admin/getCode?timestamp="+getTimestamp);
	}
</script> 
</head>
<body>
<input type="hidden" id="TenantId" name="TenantId" value="" />
<div class="header"></div>
<div class="loginWraper">
  <div id="loginform" class="loginBox">
    <form class="form form-horizontal" name="theform" id="theform" method="post">
      <div class="row cl">
        <label class="form-label col-3" style="float: left;"><i class="Hui-iconfont">&#xe60d;</i></label>
        <div class="formControls col-8" style="float: left;">
          <input id="loginName" name="loginName" type="text" placeholder="账户" class="input-text size-L">
        </div>
      </div>
      <div class="row cl">
        <label class="form-label col-3" style="float: left;"><i class="Hui-iconfont">&#xe60e;</i></label>
        <div class="formControls col-8" style="float: left;">
          <input id="password" name="password" type="password" placeholder="密码" class="input-text size-L">
        </div>
      </div>
      <div class="row cl">
        <div class="formControls col-8 col-offset-3">
          <input class="input-text size-L" name="logincode" type="text" placeholder="验证码"   style="width:150px;">
          <img id="code" src="${path }/admin/getCode"> <a id="kanbuq" href="javascript:changeCode()">看不清，换一张</a> </div>
      </div>
      <div class="row">
        <div class="formControls col-8 col-offset-3">
          <label id="errorMsg" style="color: red;"></label>
        </div>
      </div>
      <div class="row">
        <div class="formControls col-8 col-offset-3">
          <input  type="button" onclick="login()" class="btn btn-success radius size-L" value="&nbsp;登&nbsp;&nbsp;&nbsp;&nbsp;录&nbsp;">
          <input  type="reset"  class="btn btn-default radius size-L" value="&nbsp;取&nbsp;&nbsp;&nbsp;&nbsp;消&nbsp;">
        </div>
      </div>
    </form>
  </div>
</div>
<div class="footer">Copyright@付苗</div>
</body>
</html>